FROM golang:buster as builder

ENV GOOS linux
ENV GOARCH amd64

RUN apt-get update && apt-get install -y libsdl2-dev libsdl2-image-dev libsdl2-ttf-dev

WORKDIR /go/src/gitlab.com/bpowell/lifeAdventure/
COPY . .

RUN go get && go build


FROM google/cloud-sdk
RUN apt-get update && apt-get install -y zip
WORKDIR /app/lifeAdventure
COPY --from=builder /go/src/gitlab.com/bpowell/lifeAdventure/lifeAdventure .
COPY assets assets

WORKDIR /app
COPY bin/push.sh .
RUN zip -r game.zip lifeAdventure
ENTRYPOINT ["./push.sh"]
