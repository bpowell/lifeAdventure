#!/bin/bash

gsutil cp game.zip gs://lifeadventure/${ENV}/lifeAdventure-${BRANCH}-${COMMIT}.zip
gsutil cp game.zip gs://lifeadventure/${ENV}/lifeAdventure-latest.zip
