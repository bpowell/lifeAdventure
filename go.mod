module gitlab.com/bpowell/lifeAdventure

go 1.13

require (
	github.com/konsorten/go-windows-terminal-sequences v1.0.1
	github.com/sirupsen/logrus v1.2.0
	github.com/veandco/go-sdl2 v0.4.0
	golang.org/x/crypto v0.0.0-20181112202954-3d3f9f413869
	golang.org/x/sys v0.0.0-20181107165924-66b7b1311ac8
)
