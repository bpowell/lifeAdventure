package main

import (
	"fmt"

	"gitlab.com/bpowell/lifeAdventure/pkg/game"
)

func main() {
	fmt.Println("vim-go")
	g := game.NewGame()
	g.Loop()
}
