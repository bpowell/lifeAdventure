package graphics

import (
	"bytes"
	"fmt"
	"gitlab.com/bpowell/lifeAdventure/pkg/graphics/sprite"
	"gitlab.com/bpowell/lifeAdventure/pkg/graphics/tile"
	"os"

	"github.com/veandco/go-sdl2/sdl"
)

type newcharacter struct {
	tile     tile.Tile
	location sdl.Rect
	picked   bool
}

func (n *newcharacter) render(s *sprite.SpriteSheet) {
	if n.picked {
		s.RenderWithSelected(n.tile, n.location)
	} else {
		s.RenderWith(n.tile, n.location)
	}
}

func (n *newcharacter) isClicked(event *sdl.MouseButtonEvent) bool {
	//should be left clicked
	switch event.Button {
	case sdl.BUTTON_RIGHT:
		return false
	}

	//should be pressed
	switch event.State {
	case sdl.RELEASED:
		return false
	}

	p := sdl.Point{event.X, event.Y}
	if !n.picked {
		n.picked = p.InRect(&n.location)
	}

	return n.picked
}

func (g *Graphics) CreateBackDrop() *sdl.Texture {
	old := g.Renderer.GetRenderTarget()
	comb, err := g.Renderer.CreateTexture(sdl.PIXELFORMAT_RGBA8888, sdl.TEXTUREACCESS_TARGET, 1280, 1024)
	if err != nil {
		panic(err)
	}

	g.Renderer.SetRenderTarget(comb)
	f := GetFonts()

	msgTexture := f.NewText(32, "Create A Character", sdl.Color{255, 233, 0, 255})
	wizardTexture := f.NewText(32, "Wizard", sdl.Color{255, 233, 0, 255})
	warriorTexture := f.NewText(32, "Warrior", sdl.Color{255, 233, 0, 255})

	g.Renderer.Copy(msgTexture.Render(500, 10))
	g.Renderer.Copy(wizardTexture.Render(430, 110))
	g.Renderer.Copy(warriorTexture.Render(430, 210))

	msgTexture.Destroy()
	wizardTexture.Destroy()
	warriorTexture.Destroy()

	g.Renderer.SetRenderTarget(old)
	return comb
}

func (g *Graphics) CharacterSelection() {
	newOptions := []*newcharacter{
		{
			tile:     tile.Tiles["MALE_WIZARD"],
			location: sdl.Rect{570, 100, 64, 64},
		},
		{
			tile:     tile.Tiles["FEMALE_WIZARD"],
			location: sdl.Rect{640, 100, 64, 64},
		},
		{
			tile:     tile.Tiles["MALE_WARRIOR"],
			location: sdl.Rect{570, 200, 64, 64},
		},
		{
			tile:     tile.Tiles["FEMALE_WARRIOR"],
			location: sdl.Rect{640, 200, 64, 64},
		},
	}

	isPicked := false
	name := bytes.NewBufferString("")

	background := g.CreateBackDrop()
	a := func() bool {
		fmt.Println("Clicked!!!")
		return true
	}

	createBtn := g.CreateTextColorButton("Create", sdl.Color{0, 0, 0, 255}, sdl.Color{211, 211, 211, 255}, 550, 755, a)

	var nameTexture *Text
	f := GetFonts()

Render:
	for {
		for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
			switch t := event.(type) {
			case *sdl.QuitEvent:
				os.Exit(0)
			case *sdl.KeyboardEvent:
				fmt.Printf("[%d ms] Keyboard\ttype:%d\tsym:%c\tmodifiers:%d\tstate:%d\trepeat:%d\n",
					t.Timestamp, t.Type, t.Keysym.Sym, t.Keysym.Mod, t.State, t.Repeat)
				if isPicked && t.State == 1 && name.Len() <= 8 {
					name.WriteString(sdl.GetKeyName(t.Keysym.Sym))
					fmt.Println(name.String())
					nameTexture.Destroy()
					nameTexture = f.NewText(32, name.String(), sdl.Color{255, 0, 0, 255})
				}
			case *sdl.MouseButtonEvent:
				if isPicked {
					b := createBtn.Clicked(t)
					switch v := b.(type) {
					case bool:
						if v {
							break Render
						}
					}
				}

				for _, n := range newOptions {
					p := n.isClicked(t)
					if p {
						isPicked = true
						for _, nn := range newOptions {
							if n != nn {
								nn.picked = false
							}
						}
					}
				}
			}
		}

		g.Renderer.Clear()
		g.Renderer.Copy(background, nil, nil)
		for _, n := range newOptions {
			n.render(g.SpriteSheet)
		}

		if isPicked {
			g.Renderer.SetDrawColor(211, 211, 211, 255)
			g.Renderer.FillRect(&sdl.Rect{470, 700, 330, 50})
			g.Renderer.Copy(nameTexture.Render(470, 700))
			g.Renderer.SetDrawColor(0, 0, 0, 255)
			g.Renderer.Copy(createBtn.Render())
		}

		g.Renderer.Present()
	}

	fmt.Println("DONE!")
}
