package sprite

import (
	"gitlab.com/bpowell/lifeAdventure/pkg/graphics/tile"
	"gitlab.com/bpowell/lifeAdventure/pkg/log"

	"github.com/veandco/go-sdl2/img"
	"github.com/veandco/go-sdl2/sdl"
)

type SpriteSheet struct {
	renderer *sdl.Renderer
	Texture  *sdl.Texture
	width    int32
	height   int32
}

func NewSpriteSheet(renderer *sdl.Renderer, filename string, width, height int32) *SpriteSheet {
	image, err := img.Load(filename)
	if err != nil {
		log.Error(err.Error())
		panic(err)
	}

	texture, err := renderer.CreateTextureFromSurface(image)
	if err != nil {
		log.Error(err.Error())
		panic(err)
	}

	return &SpriteSheet{
		renderer: renderer,
		Texture:  texture,
		width:    width,
		height:   height,
	}
}

func (sh *SpriteSheet) Render() {
	//Wizard f(44, 77), m(49, 76)
	//Warrior m(48, 76), f(43, 77)
	sh.renderer.Copy(sh.Texture, &sdl.Rect{43 * 32, 77 * 32, sh.width, sh.height}, &sdl.Rect{0, 0, 64, 64})
}

func (sh *SpriteSheet) RenderWith(tile tile.Tile, loc sdl.Rect) {
	sh.renderer.Copy(sh.Texture, &sdl.Rect{tile.X * sh.width, tile.Y * sh.height, sh.width, sh.height}, &loc)
}

func (sh *SpriteSheet) RenderWithSelected(tile tile.Tile, loc sdl.Rect) {
	sh.renderer.SetDrawColor(255, 0, 0, 255)
	sh.renderer.DrawRect(&loc)
	sh.renderer.SetDrawColor(0, 0, 0, 255)
	sh.RenderWith(tile, loc)
}
