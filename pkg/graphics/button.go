package graphics

import (
	"github.com/veandco/go-sdl2/sdl"
)

type Button struct {
	Callback interface{}
	Texture  *sdl.Texture
	dst      sdl.Rect
}

func (g *Graphics) CreateTextColorButton(msg string, textColor sdl.Color, btnColor sdl.Color, x, y int, callback interface{}) *Button {
	old := g.Renderer.GetRenderTarget()

	f := GetFonts()
	font := f.NewText(32, msg, textColor)

	comb, err := g.Renderer.CreateTexture(sdl.PIXELFORMAT_RGBA8888, sdl.TEXTUREACCESS_TARGET, font.Width, font.Height)
	if err != nil {
		panic(err)
	}

	g.Renderer.SetRenderTarget(comb)
	g.Renderer.SetDrawColor(btnColor.R, btnColor.G, btnColor.B, btnColor.A)
	g.Renderer.FillRect(&sdl.Rect{0, 0, font.Width, font.Height})
	g.Renderer.Copy(font.Texture, nil, nil)

	g.Renderer.SetRenderTarget(old)

	return &Button{Callback: callback, Texture: comb, dst: sdl.Rect{int32(x), int32(y), font.Width, font.Height}}
}

func (b *Button) Render() (*sdl.Texture, *sdl.Rect, *sdl.Rect) {
	return b.Texture, nil, &b.dst
}

func (b *Button) Clicked(event *sdl.MouseButtonEvent) interface{} {
	switch event.Button {
	case sdl.BUTTON_RIGHT:
		return false
	}

	//should be pressed
	switch event.State {
	case sdl.RELEASED:
		return false
	}

	p := sdl.Point{event.X, event.Y}
	if p.InRect(&b.dst) {
		switch v := b.Callback.(type) {
		case func() bool:
			return v()
		}
	}

	return false
}
