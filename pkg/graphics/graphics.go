package graphics

import (
	"gitlab.com/bpowell/lifeAdventure/pkg/graphics/sprite"
	"gitlab.com/bpowell/lifeAdventure/pkg/log"

	"github.com/veandco/go-sdl2/sdl"
)

type Graphics struct {
	Window      *sdl.Window
	Renderer    *sdl.Renderer
	SpriteSheet *sprite.SpriteSheet
	Mobs        []*Mob
}

var graphicsInstance *Graphics

func init() {
	log.Info("Starting up...")
	if err := sdl.Init(sdl.INIT_EVENTS | sdl.INIT_VIDEO); err != nil {
		log.Error("Failed on sdl.Init")
		log.Error(err.Error())
		panic(err)
	}
	log.Info("SDL setup")

	window, err := sdl.CreateWindow("test", sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED,
		1280, 1024, sdl.WINDOW_SHOWN)
	if err != nil {
		log.Error("Cannot create sdl window")
		log.Error(err.Error())
		panic(err)
	}
	log.Info("Created SDL window")

	renderer, err := sdl.CreateRenderer(window, -1, sdl.RENDERER_ACCELERATED)
	if err != nil {
		log.Error("Cannot create sdl renderer")
		log.Error(err.Error())
		panic(err)
	}
	log.Info("Created SDL renderer")

	spritesheet := sprite.NewSpriteSheet(renderer, "assets/sheet.png", 32, 32)
	log.Info("Loaded assets")

	graphicsInstance = &Graphics{
		Window:      window,
		Renderer:    renderer,
		SpriteSheet: spritesheet,
	}
}

func GetGraphics() *Graphics {
	return graphicsInstance
}

func (g *Graphics) AddMob(m *Mob) {
	g.Mobs = append(g.Mobs, m)
}

func (g *Graphics) Render() {
	g.Renderer.Clear()
	for _, m := range g.Mobs {
		m.Render()
	}
	g.Renderer.Present()
}
