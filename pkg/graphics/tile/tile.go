package tile

import (
	"encoding/json"
	"io/ioutil"
)

type Tile struct {
	X int32
	Y int32
}

var Tiles map[string]Tile

func init() {
	f, err := ioutil.ReadFile("assets/tiles.json")
	if err != nil {
		panic(err)
	}

	if err := json.Unmarshal(f, &Tiles); err != nil {
		panic(err)
	}
}
