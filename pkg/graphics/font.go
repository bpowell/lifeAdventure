package graphics

import (
	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/ttf"
)

type Text struct {
	Message string
	Width   int32
	Height  int32
	Texture *sdl.Texture
}

type Font struct {
	Fonts map[int]*ttf.Font
}

var fontInstance *Font

func GetFonts() *Font {
	return fontInstance
}

func init() {
	if err := ttf.Init(); err != nil {
		panic(err)
	}

	fonts := make(map[int]*ttf.Font, 2)
	font32, err := ttf.OpenFont("assets/font.ttf", 32)
	if err != nil {
		panic(err)
	}
	fonts[32] = font32

	font16, err := ttf.OpenFont("assets/font.ttf", 16)
	if err != nil {
		panic(err)
	}
	fonts[16] = font16

	font24, err := ttf.OpenFont("assets/font.ttf", 24)
	if err != nil {
		panic(err)
	}
	fonts[24] = font24

	font8, err := ttf.OpenFont("assets/font.ttf", 8)
	if err != nil {
		panic(err)
	}
	fonts[8] = font8

	fontInstance = &Font{
		Fonts: fonts,
	}
}

func (f *Font) NewText(size int, msg string, color sdl.Color) *Text {
	surface, err := f.Fonts[size].RenderUTF8Solid(msg, color)
	if err != nil {
		panic(err)
	}

	defer surface.Free()

	texture, err := graphicsInstance.Renderer.CreateTextureFromSurface(surface)
	if err != nil {
		panic(err)
	}

	w, h, err := f.Fonts[size].SizeUTF8(msg)
	if err != nil {
		panic(err)
	}

	return &Text{Message: msg, Width: int32(w), Height: int32(h), Texture: texture}
}

func (t *Text) Render(x, y int32) (*sdl.Texture, *sdl.Rect, *sdl.Rect) {
	if t == nil {
		return nil, nil, nil
	}

	src := sdl.Rect{0, 0, t.Width, t.Height}
	dst := sdl.Rect{x, y, t.Width, t.Height}

	return t.Texture, &src, &dst
}

func (t *Text) Destroy() {
	if t == nil {
		return
	}

	if t.Texture != nil {
		t.Texture.Destroy()
	}
}
