package graphics

import (
	"fmt"
	"gitlab.com/bpowell/lifeAdventure/pkg/graphics/sprite"
	"gitlab.com/bpowell/lifeAdventure/pkg/graphics/tile"

	"github.com/veandco/go-sdl2/sdl"
)

type Mob struct {
	sprite     *sprite.SpriteSheet
	loc        sdl.Point
	tile       tile.Tile
	damageDone int
	inCombat   bool
}

func NewMob(s *sprite.SpriteSheet, t tile.Tile) *Mob {
	return &Mob{sprite: s, tile: t, loc: sdl.Point{200, 200}, inCombat: true, damageDone: 5}
}

func (m *Mob) Render() {
	if m.inCombat {
		dmgMsg := fontInstance.NewText(24, fmt.Sprintf("%d", m.damageDone), sdl.Color{255, 0, 0, 255})
		graphicsInstance.Renderer.Copy(dmgMsg.Render(m.loc.X+15, m.loc.Y-35))
		dmgMsg.Destroy()
	}

	m.sprite.RenderWith(m.tile, sdl.Rect{m.loc.X, m.loc.Y, 32, 32})
}
