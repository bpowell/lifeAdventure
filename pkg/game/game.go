package game

import (
	"gitlab.com/bpowell/lifeAdventure/pkg/graphics"
	"gitlab.com/bpowell/lifeAdventure/pkg/graphics/tile"
)

type Game struct {
	UI *graphics.Graphics
}

func NewGame() *Game {
	return &Game{
		UI: graphics.GetGraphics(),
	}
}

func (g *Game) Loop() {
	g.UI.CharacterSelection()
	m := graphics.NewMob(g.UI.SpriteSheet, tile.Tiles["MALE_WIZARD"])
	g.UI.AddMob(m)
	for {
		g.UI.Render()
	}
}
