package player

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

type Player struct {
	Name   string
	TileId string
}

func LoadPlayers() []Player {
	var players []Player
	file, err := ioutil.ReadFile("players.save")

	if err != nil {
		fmt.Println("No save file found")
		return players
	}

	if err := json.Unmarshal(file, &players); err != nil {
		panic(err)
	}

	return players
}

func (p Player) Save() {
	var players []Player
	file, err := ioutil.ReadFile("players.save")

	if err == nil {
		if err := json.Unmarshal(file, &players); err != nil {
			panic(err)
		}
	}

	players = append(players, p)

	b, err := json.Marshal(players)
	if err != nil {
		panic(err)
	}

	ioutil.WriteFile("players.save", b, 0644)
}
