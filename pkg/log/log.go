package log

import (
	"os"

	"github.com/sirupsen/logrus"
)

var logger = logrus.New()

func init() {
	f, err := os.OpenFile("game.log", os.O_WRONLY|os.O_CREATE, 0644)
	if err != nil {
		panic(err)
	}

	logger.SetOutput(f)
}

func Error(args ...interface{}) {
	logger.Error(args...)
}

func Info(args ...interface{}) {
	logger.Info(args...)
}
